var express = require('express');
var router = express.Router();

var services = 
[
  {"name": "Security V2", "address": "inpsol08.is.inps.co.uk:80/"},
  {"name": "Community", "address": "172.17.186.222:7445/"},
  {"name": "Example2", "address": "172.17.186.223:7446/"},
  {"name": "Example3", "address": "172.17.186.224:7447/"},
  {"name": "Example4", "address": "172.17.186.225:7448/"},
  {"name": "Example5", "address": "172.17.186.226:7449/"},
  ]


/* GET status page. */
router.get('/', function(req, res, next) {
  res.render('statusViewer', { title: 'Services', serviceList : services });
});

module.exports = router;