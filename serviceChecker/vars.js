module.exports = {
    testService: function (paramHost, paramPort, paramPath) {
        var options = {
            host: paramHost,
            port: paramPort,
            path: paramPath
        };

        var http = require('http');

        console.log("Testing : " + paramHost + ':' + paramPort + paramPath);

        http.get(options, function (res) {
            if (res.statusCode == 200) {
                console.log("success");
            }
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        });

        function testUrl(port, host, cb) {
            http.get({
                host: host,
                port: port
            }, function (res) {
                cb("Available", res);
            }).on("error", function (e) {
                cb("Unavailable", e);
            });
        }
    }
}